from client_server.client import Client


def start_client():
    """ Method starts client connection """
    client = Client()
    client.build_connection()

if __name__ == "__main__":
    start_client()
