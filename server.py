from client_server.connection import Connection
from client_server.delivery import Delivery
from socket import error as SocketError
import errno
from sys import getsizeof


class Server:
    """ Class contains server settings """

    conn_num = 0

    def build_connection(self):
        """ Method starts server """
        sock = Connection().SOCK
        Connection().server_connection_setup("127.0.0.1", 8000)
        print("Connection waiting...")

        try:
            conn, client = sock.accept()
            while True:
                self.conn_num += 1
                print("[+] Connection #%d established..." % self.conn_num)
                chunk = Delivery().receive(conn)
                print(chunk)
                print(type(chunk))
                self.send_info(chunk, conn)
                print("[+] File was downloaded.")

        except SocketError as e:
            if e.errno != errno.ECONNRESET:
                pass

    def send_info(self, chunk, connection):
        """ Method treys to send check info """
        data_size = getsizeof(chunk)
        print("data %d" % data_size)
        print(type(data_size))
        Delivery().send(b"%d" % data_size, connection)
