from client_server.server import Server


def start_server():
    """ Method starts server connection """
    server = Server()
    server.build_connection()

if __name__ == "__main__":
    start_server()
