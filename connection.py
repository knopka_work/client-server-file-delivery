import socket


class Connection:
    """ Class contains settings for client-server connection """
    SOCK = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def server_connection_setup(self, ip, port):
        """ Method setups client-server connection
        ip=Internet Protocol of Server
        port=Control Protocol for HTTP connection
        """
        while True:
            try:
                self.SOCK.bind((ip, port))
                self.SOCK.listen(5)
                break
            except socket.error as err:
                print("Error while connecting %s ..." % err)
                self.close()

    def client_connection_setup(self, ip, port):
        """ Method setups client-server connection
        ip=Internet Protocol of Server
        port=Control Protocol for HTTP connection
        """
        while True:
            try:
                self.SOCK.connect((ip, port))
                break
            except socket.error as err:
                print("Error while connecting %s ..." % err)
                self.close()

    def close(self):
        """ Method closes socket """
        self.SOCK.close()
