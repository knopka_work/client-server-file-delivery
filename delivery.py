


class Delivery:
    """ Class contains method for delivery messages """

    def send(self, delivery_obj, connection):
        """ Method contains settings for send data """
        print("[...] File sending...")
        # data = delivery_obj.encode("utf-8")
        connection.send(delivery_obj)

    def receive(self, connection):
        """ Method contains settings for receive data """
        data = connection.recv(1024)
        # chunk = data.decode("utf-8")
        return data
