from client_server.connection import Connection
from client_server.delivery import Delivery
from functools import partial
from sys import getsizeof


class Client:
    """ Class contains client settings """
    file = "file"
    count = 0

    def build_connection(self):
        """ Method starts client """
        conn = Connection().SOCK
        Connection().client_connection_setup("127.0.0.1", 8000)
        print("Trying to build connection...")

        self.send_file(conn)
        # self.check_file(conn)

        conn.close()

    def send_file(self, connection):
        """ Method treys to send file """
        with open(self.file, "rb") as f:
            for chunk in iter(partial(f.read, 1024), b''):
                self.count += 1
                print(self.count)
                chunk_size = getsizeof(chunk)
                print("chunk %d" % chunk_size)
                Delivery().send(chunk, connection)

                data = Delivery().receive(connection)
                answ_size = int(data)
                print("answ %s" % answ_size)
                print("[+] Answer was received.")

                if answ_size != chunk_size:
                    print("[...] File resending was began...")
                    Delivery().send(chunk, connection)
                    print("[+] Sending was complete...")
                else:
                    print("[+] File delivery was complete with success.")
